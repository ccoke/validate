## 项目简介
**Validate**是一款基于Spring Boot的数据验证框架，它能够让Web数据验证变地更加简单，编程人员只需关注代码逻辑而不需要在数据验证上花费过多的时间，并且使用它可以避免大量的重复代码与空指针异常。

## 功能介绍
+ 非空校验
+ 四种验证模式
+ 正则匹配

#### 非空校验
本框架编写之初，便是为了解决Web请求参数非空校验，在参数监测时，对于String类型属性，会同时检验是否为NULL或"",对于非String类型属性，只判断是否为NULL。

#### 四种验证模式
一般来说，用户请求参数很多时候都是不定的，如可以在参数上使用**required = false**
申明这是一个非必要参数;还有一种情况是，对于JSON请求，与之对应的类中属性一般都比JSON参数多，所以需要开发人员自己确定接口哪些参数需要验证。我们为开发人员选择了四种参数选择模式：
1. ALL - 验证所有参数
2. INCLUDE - 验证指定名称参数
3. IGNORE - 验证除了指定名称之外的所有参数
4. IGNORE_ANNOTATION - 验证除了被注解标识参数的所有参数

#### 正则匹配
对于一款校验框架来说，光判断参数是否为空还是远远不够的，我们还为其添加了正则匹配，用户可为被测参数指定匹配格式，我们内置了几种常见的数据格式方便开发人员上手即用，另外，我们结合了Spring Boot的配置形式，方便开发人员定义自己的数据格式，并且，如果您觉得自己的我们给的数据格式不符合您的实际需要，您可以很方便地覆写默认的格式。

|格式名称|说明|
|:------:|:------:|
|EMAIL|电子邮件地址|
|URL|网页链接|
|QQ|QQ号码|
|POSTAL_CODE|邮编|
|ID_NUMBER|身份证号码|

注: 如需要使用或覆写，请注意格式名称的大小写。以上正则来源于http://tool.oschina.net/regex/

## 使用方法
#### 获取框架
1. 从Maven导入
```
<dependency>
    <groupId>cc.ccoke</groupId>
    <artifactId>validate-spring-boot-starter</artifactId>
    <version>0.4</version>
</dependency>
```
2. 手动下载jar包

+ validate-spring-boot-starter-0.5.jar [点击下载](https://gitee.com/ccoke/validate/attach_files/download?i=194525&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F06%2F01%2FPaAvDFwcokCAPwenAABZL07tClg647.jar%3Ftoken%3D0e17d4e65ede5aee80230f73e0dfaf14%26ts%3D1545380416%26attname%3Dvalidate-spring-boot-starter-0.5.jar)

#### 在Spring Boot中启用框架
在Application中添加 **@EnableValidat** 注解便可启用。
```
@SpringBootApplication
@EnableValidate
public class TestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestApplication.class, args);
	}
}
```
#### 基本使用
1. 判断方法所有参数

    通过在控制类方法上添加 **@ParamVerify** 注解或 **@ParamVerify(type = VerifyType.ALL)** 注解，便可判断所有参数是否为空。
    ```
    @RestController
    public class HelloController {
        @PostMapping("/hi")
        @ParamVerify
        public String hi(@RequestParam String hi, @RequestBody Student student){
            return hi + student.getName();
        }
    }
    
    ```

2. 判断指定名称参数是否为空

    通过在控制类方法上添加 **@ParamVerify(type = VerifyType.INCLUDE, value = {})** 注解，便可判断所有参数是否为空。如果需要判断自定义类中的属性，需要将 **对象名** 与 **对象名.属性名** 一起加入到value中。
    ```
    @RestController
    public class HelloController {
        @PostMapping("/hi")
        @ParamVerify(type = VerifyType.INCLUDE, value = {"hi", "student", "student.name"})
        public String hi(@RequestParam String hi, @RequestBody Student student){
            return hi + student.getName();
        }
    }
    
    ```

3. 验证除了指定名称之外的所有参数

    通过在控制类方法上添加 **@ParamVerify(type = VerifyType.IGNORE, value = {})** 注解，便可判断除了指定名称之外的所有参数是否为空。
    ```
    @RestController
    public class HelloController {
        @PostMapping("/hi")
        @ParamVerify(type = VerifyType.IGNORE, value = {"student"， "student.name"})
        public String hi(@RequestParam String hi, @RequestBody Student student){
            return hi + student.getName();
        }
    }
    ```
4. 验证除了被注解标识参数的所有参数

    在自定义类属性上添加 **@IgnoreVerify** 便可在参数监测时跳过该参数，该注解需要配合 **@ParamVerify(type = VerifyType.IGNORE_ANNOTATION)** 一起使用。
    ```
    public class Student {
        @IgnoreVerify
        private String email;
        private String name;
    
        public Student(String email, String name) {
            this.email = email;
            this.name = name;
        }
        // 省略Setter和Getter...
    }
    
    ```
5. 正则匹配

    在目标参数上添加 **@ParamType("")** 注解，便可在监测时进行正则匹配。
    ```
    public class Student {
        @ParamType(value="EMAIL")
        // 或者 @ParamType("EMAIL")
        private String email;
        @ParamType(regex="^\\S{2,8}$", describe="姓名长度必须为2到8位")
        private String name;
    
        public Student(String email, String name) {
            this.email = email;
            this.name = name;
        }
        // 省略Setter和Getter...
    }
    
    ```
    ```
    @RestController
    public class HelloController {
        @PostMapping("/hi")
        @ParamVerify
        public String hi(@RequestParam @ParamType("QQ") String qq, @RequestBody Student student){
            return qq + student.getEmail();
        }
    }
    ```
    当 **@ParamType** 注解中同时定义了value和regex,会优先匹配 **regex**。字段value、regex二选一，describe选填。

6. 自定义格式

    在SpringBoot配置文件(yaml或properties)中，添加如下配置参数，便可进行正则覆写与扩充。需要注意的是，Java的转义字符需要使用双"\\"。
    ```
    cc:
      ccoke:
        validate:
          regulars:
            - {name: "EMAIL", regex: "\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?", describe: ""}
            - {name: "URL", regex: "[a-zA-z]+://[^\\s]*", describe: "" }
            - {name: "MORE", regex: "....", describe: ""}
    ```
## 更新日志
#### 0.2 
+ 修复了 **IgnoreParamVerify** 模式下，不能对自定义对象属性进行检验的BUG

#### 0.3
+ 修复了验证自定义对象属性时没有判断自定义对象是否为空的BUG
+ 修改了配置文件参数名称，将pattern 修改为 regex
+ 修改了代码格式，通过了阿里编码规范扫描

#### 0.5
+ 为了优化异常提醒，在规则添加了**describe**字段
+ 增强了 **@ParamType**注解，可以在注解参数时添加规则正则与描述

## 常见问题
> 目前框架还是最原始的版本，可能还存在着许多未知的BUG与功能的不足，希望大家在使用中发现了问题或有什么建议或意见，能及时与我们沟通或者加入到框架的开发中，我们坚信，我们的努力会让本它成为一款Java开发利器。

+ 为什么将框架添加到项目后，没有生效？
    1. Application中是否使用 **@EnableValidat** 注解开启框架?
    2. **@ParamVerify** 注解是否应用在控制类中?
    3. Application中使用了 **@ComponentScan** 进行Bean扫描，如果有，需要将 **cc.ccoke.validate** 加入其中。