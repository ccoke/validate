package cc.ccoke.validate.annotation;

import cc.ccoke.validate.config.ValidateAutoConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 在Spring Boot中使用该注解加载框架
 * @author ccoke
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(ValidateAutoConfig.class)
@Documented
public @interface EnableValidate {
}
