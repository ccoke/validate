package cc.ccoke.validate.annotation;


import cc.ccoke.validate.type.VerifyType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 数据验证注解
 * @author ccoke
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ParamVerify {
    // 验证类型
    VerifyType type() default VerifyType.ALL;

    // 需要验证的属性名称
    String[] value() default {};
}
