package cc.ccoke.validate.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 申明参数类型注解
 * @author ccoke
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ParamType {
    // 类型名称
    String value() default "";

    // 规则
    String regex() default "";

    // 规则描述
    String describe() default "";
}
