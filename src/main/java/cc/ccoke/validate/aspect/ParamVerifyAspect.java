package cc.ccoke.validate.aspect;

import cc.ccoke.validate.annotation.ParamVerify;
import cc.ccoke.validate.verify.*;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

/**
 * 分类请求数据验证
 * @author ccoke
 */
@Aspect
public class ParamVerifyAspect {
    @Around("@annotation(paramVerify)")
    public Object doAround(ProceedingJoinPoint pjp, ParamVerify paramVerify) throws Throwable {
        Signature signature = pjp.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Object[] args = pjp.getArgs();
        BaseParamVerify baseParamVerify = null;
        switch (paramVerify.type()){
            case INCLUDE:
                baseParamVerify = new IncludeParamVerify();
                break;
            case IGNORE:
                baseParamVerify = new IgnoreParamVerify();
                break;
            case IGNORE_ANNOTATION:
                baseParamVerify = new IgnoreAnnotationParamVerify();
                break;
            default:
                baseParamVerify = new AllParamVerify();
                break;
        }
        baseParamVerify.doVerify(args, methodSignature, paramVerify.value());
        return pjp.proceed();
    }

}
