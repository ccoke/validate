package cc.ccoke.validate.config;

import cc.ccoke.validate.aspect.ParamVerifyAspect;
import cc.ccoke.validate.entity.ValidateProperties;
import cc.ccoke.validate.factory.RegularFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
* @author: ccoke
*/
@Configuration
@EnableConfigurationProperties(ValidateProperties.class)
@ConditionalOnClass(ParamVerifyAspect.class)
public class ValidateAutoConfig {

    @Autowired
    private ValidateProperties validateProperties;

    @Bean
    @ConditionalOnMissingBean(ParamVerifyAspect.class)
    public ParamVerifyAspect paramVerifyAspect() {
        // 设置规则词典
        RegularFactory.setRegulars(validateProperties.getRegulars());
        // 将Aspect生成Bean
        return new ParamVerifyAspect();
    }

}
