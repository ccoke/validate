package cc.ccoke.validate.type;

/**
 * 验证类型
 * @author ccoke
 */
public enum VerifyType {
    // 验证所有
    ALL,
    // 涵盖的参数名
    INCLUDE,
    // 除了参数名之外的
    IGNORE,
    // 适用除了参数之外的注解
    IGNORE_ANNOTATION;
}
