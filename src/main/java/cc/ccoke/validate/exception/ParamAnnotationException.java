package cc.ccoke.validate.exception;

/**
 * 注解内Value值异常
 * @author ccoke
 */
public class ParamAnnotationException extends ParamException {

    public ParamAnnotationException(String message) {
        super(message);
    }
}
