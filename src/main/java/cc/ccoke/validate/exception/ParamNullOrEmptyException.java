package cc.ccoke.validate.exception;

/**
 * 对象为空异常
 * @author ccoke
 */
public class ParamNullOrEmptyException extends ParamException {
    private static final String MESSAGE = " can not be null or empty";
    public ParamNullOrEmptyException(String paramName) {
        super(paramName + MESSAGE);
    }
}
