package cc.ccoke.validate.exception;

/**
 * 参数异常
 * @author ccoke
 */
public class ParamException extends RuntimeException {
    public ParamException(String message) {
        super(message);
    }
}
