package cc.ccoke.validate.exception;

/**
 * 对象不符合规则异常
 * @author ccoke
 */
public class ParamRegularException extends ParamException {
    public ParamRegularException(String paramName, String regularName) {
        super(paramName + ": " + regularName);
    }
}
