package cc.ccoke.validate.verify;

import cc.ccoke.validate.exception.ParamNullOrEmptyException;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * 忽略指定参数
 * @author ccoke
 */
public class IgnoreParamVerify extends BaseParamVerify {
    @Override
    public void doVerify(Object[] args, MethodSignature methodSignature, String[] value) throws IllegalAccessException {
        Annotation[][] annotations = methodSignature.getMethod().getParameterAnnotations();
        for(int i=0;i<args.length;i++){
            String parameterName = methodSignature.getParameterNames()[i];
            if (isItemInArray(parameterName, value)) {
                continue;
            }
            if (isEmpty(args[i])){
                throw new ParamNullOrEmptyException(parameterName);
            }
            checkParamType(annotations, i, args[i], parameterName);
            if (isBasicType(args[i].getClass().getTypeName())) {
                continue;
            }
            Field[] fields = args[i].getClass().getDeclaredFields();
            for (int j = 0; j < fields.length; j++) {
                fields[j].setAccessible(true);
                String fieldName = parameterName + "." + fields[j].getName();
                if (isItemInArray(fieldName, value)) {
                    continue;
                }
                if (isEmpty(fields[j].get(args[i]))){
                    throw new ParamNullOrEmptyException(fieldName);
                }
                checkParamType(fields[j], args[i], parameterName);
            }
        }
    }
}
