package cc.ccoke.validate.verify;

import cc.ccoke.validate.exception.ParamNullOrEmptyException;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * 验证所有参数
 * @author ccoke
 */
public class AllParamVerify extends BaseParamVerify {
    @Override
    public void doVerify(Object[] args, MethodSignature methodSignature, String[] value) throws IllegalAccessException {
        // 获取方法上所有注解
        Annotation[][] annotations = methodSignature.getMethod().getParameterAnnotations();
        for(int i=0;i<args.length;i++){
            String parameterName = methodSignature.getParameterNames()[i];
            if (isEmpty(args[i])){
                throw new ParamNullOrEmptyException(parameterName);
            }
            // 检验参数规则
            checkParamType(annotations, i, args[i], parameterName);
            // 如果是基本类型，则不深入判断
            if (isBasicType(args[i].getClass().getTypeName())) {
                continue;
            }
            Field[] fields = args[i].getClass().getDeclaredFields();
            for (int j = 0; j < fields.length; j++) {
                //允许访问静态变量值
                fields[j].setAccessible(true);
                if (isEmpty(fields[j].get(args[i]))){
                    throw new ParamNullOrEmptyException( parameterName + "." + fields[j].getName());
                }
                checkParamType(fields[j], args[i], parameterName);
            }
        }
    }


}
