package cc.ccoke.validate.verify;


import cc.ccoke.validate.annotation.IgnoreVerify;
import cc.ccoke.validate.exception.ParamNullOrEmptyException;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

/**
 * 忽略带注解的参数
 * @author ccoke
 */
public class IgnoreAnnotationParamVerify extends BaseParamVerify {
    @Override
    public void doVerify(Object[] args, MethodSignature methodSignature, String[] value) throws IllegalAccessException {
        Annotation[][] annotations = methodSignature.getMethod().getParameterAnnotations();
        for(int i=0;i<args.length;i++){
            String parameterName = methodSignature.getParameterNames()[i];
            // 参数是否拥有Ignore注解
            if (haveIgnoreAnnotation(annotations[i])){
                continue;
            }
            if (isEmpty(args[i])){
                throw new ParamNullOrEmptyException(parameterName);
            }
            checkParamType(annotations, i, args[i], parameterName);

            if (isBasicType(args[i].getClass().getTypeName())) {
                continue;
            }

            Field[] fields = args[i].getClass().getDeclaredFields();
            for (int j = 0; j < fields.length; j++) {
                fields[j].setAccessible(true);
                String fieldName = parameterName + "." + fields[j].getName();
                if (fields[j].getAnnotation(IgnoreVerify.class) != null) {
                    continue;
                }
                if (isEmpty(fields[j].get(args[i]))){
                    throw new ParamNullOrEmptyException(fieldName);
                }
                checkParamType(fields[j], args[i], parameterName);
            }
        }
    }

    /**
     * 参数是否拥有Ignore注解
     * @param annotations 参数拥有的注解
     * @return true 拥有; false:未拥有
     */
    private boolean haveIgnoreAnnotation(Annotation[] annotations) {
        for (int j = 0; j < annotations.length; j++) {
            if(annotations[j].annotationType() == IgnoreVerify.class){
                return true;
            }
        }
        return false;
    }


}
