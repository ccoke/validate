package cc.ccoke.validate.entity;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * 配置文件对应参数
 * @author: ccoke
 */
@ConfigurationProperties("cc.ccoke.validate")
public class ValidateProperties {
    private List<Regular> regulars;

    public List<Regular> getRegulars() {
        return regulars;
    }

    public void setRegulars(List<Regular> regulars) {
        this.regulars = regulars;
    }
}
