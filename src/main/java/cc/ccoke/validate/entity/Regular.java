package cc.ccoke.validate.entity;

/**
 * 规则
 * @author ccoke
 */
public class Regular {
    /**
     * 名称
     */
    private String name;

    /**
     * 正则表达式
     */
    private String regex;

    /**
     * 描述
     */
    private String describe;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public Regular() {
    }

    public Regular(String regex, String describe) {
        this.regex = regex;
        this.describe = describe;
    }

    public Regular(String name, String regex, String describe) {
        this.name = name;
        this.regex = regex;
        this.describe = describe;
    }
}
